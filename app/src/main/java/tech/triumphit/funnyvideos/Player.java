package tech.triumphit.funnyvideos;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class Player extends YouTubeBaseActivity {

    String id = "";
    private YouTubePlayerView youTubeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);

        getIntent().getStringExtra("id");
        //Log.e("error", youTubeInitializationResult.toString());
        // Initializing video player with developer key
        //youTubeView.initialize("AIzaSyCqMfBCb5u5h4IAWdJ8q_ID9l4qzO7l7mY", this);
        YouTubePlayer.OnInitializedListener onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(getIntent().getStringExtra("id"));
                //youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Log.e("error", youTubeInitializationResult.toString());
            }
        };

        youTubeView.initialize("AIzaSyCqMfBCb5u5h4IAWdJ8q_ID9l4qzO7l7mY", onInitializedListener);

    }

}

