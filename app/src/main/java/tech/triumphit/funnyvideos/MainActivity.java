package tech.triumphit.funnyvideos;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.media.audiofx.Equalizer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.pushbots.push.Pushbots;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.startapp.android.publish.Ad;
import com.startapp.android.publish.AdDisplayListener;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import tech.triumphit.funnyvideos.adapter.LVAdapter;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    YouTubePlayerView youTubeView;
    String test = "http://triumphit.tech/funnyvideos/funnyvideos.php";
    private ArrayList title;
    private ArrayList lastid;
    private ArrayList like, dislike, duration, thumbnail;
    private int totalNumberOfVideos;
    private LinearLayout mLinearScroll;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private ListView lv;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList views;
    private Button previousClicked;
    int s = 0;
    int e = 50;
    private String  onRefresh = null;
    private StartAppAd startAppAd = new StartAppAd(this);
    private AnalyticsTrackers at;
    private Tracker t;
    Account account;
    private String accountName;
    private ViewFlipper iv;

    private TextView titleText, subTitle;
    private ImageView icon;

    private HorizontalScrollView scroll;

    int imageArra [] = new int [4];

    ImageView v1, v2, v3, v4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_holder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }


        //Bitmap b = Picasso.

        getSupportActionBar().setTitle("");

        initializeAllApi();

        s = sp.getInt("s", 0);
        e = sp.getInt("e", 50);

        createUser();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        scroll = (HorizontalScrollView) findViewById(R.id.scroll);

        View headerview = mNavigationView.getHeaderView(0);

        iv = (ViewFlipper) headerview.findViewById(R.id.imageSwitcher);
        icon = (ImageView) headerview.findViewById(R.id.profile_image);
        Picasso.with(this).load(Containers.ad[3]).fit().centerCrop().into(icon);
        v1 = (ImageView) iv.findViewById(R.id.v1);
        v2 = (ImageView) iv.findViewById(R.id.v2);
        v3 = (ImageView) iv.findViewById(R.id.v3);
        v4 = (ImageView) iv.findViewById(R.id.v4);

        Picasso.with(this).load(Containers.ad[4]).fit().centerCrop().into(v1);
        Picasso.with(this).load(Containers.ad[5]).fit().centerCrop().into(v2);
        Picasso.with(this).load(Containers.ad[6]).fit().centerCrop().into(v3);
        Picasso.with(this).load(Containers.ad[7]).fit().centerCrop().into(v4);

        titleText = (TextView) headerview.findViewById(R.id.username);
        subTitle = (TextView) headerview.findViewById(R.id.email);

        titleText.setText(Containers.ad[1]);
        subTitle.setText(Containers.ad[2]);
//        for(int t = 0; t < Containers.ad.length; t++){
//            Toast.makeText(this, Containers.ad[t] + "fasdf", Toast.LENGTH_SHORT).show();
//        }


        mNavigationView.setItemIconTintList(null);
        lv = (ListView) findViewById(R.id.lv);
        mLinearScroll = (LinearLayout) findViewById(R.id.linear_scroll);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);


        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        final ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                if(item.getItemId() == R.id.watched_videos){
                    startActivity(new Intent(MainActivity.this, LikedAndWatched.class).putExtra("link", "http://triumphit.tech/funnyvideos/watched.php").putExtra("title", "Watched Videos"));
                }
                if(item.getItemId() == R.id.liked_videos){
                    startActivity(new Intent(MainActivity.this, LikedAndWatched.class).putExtra("link", "http://triumphit.tech/funnyvideos/liked.php").putExtra("title", "Liked Videos"));
                }
                if(item.getItemId() == R.id.about){
                    startActivity(new Intent(MainActivity.this, About.class));
                }
                return false;
            }
        });

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Containers.ad[8]));
                startActivity(browserIntent);
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Ad " + Containers.ad[8])
                        .setAction("ad Clicked " + sp.getString("account", "notset"))
                        .setLabel("Clicked Full " + sp.getString("account", "notset") + " " + getDeviceID())
                        .build());
                sendReport();
            }
        });


        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        hit(s, e);
                                        //scroll.smoothScrollBy(sp.getInt("scroll", 0), 0);
                                    }
                                }
        );

        final Animation in = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        final Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        iv.setInAnimation(in);
        iv.setOutAnimation(out);
        iv.setFlipInterval(7500);
        iv.startFlipping();

        scroll.postDelayed(new Runnable() {
            @Override
            public void run() {
                //hsv.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                scroll.smoothScrollBy(sp.getInt("scroll", 0), 0);
            }
        },3000);

//        iv.postDelayed(new Runnable() {
//            int i = 0;
//            public void run() {
//                i++;
//                if(i > imageArra.length - 1){
//                    i = 0;
//                }
//                if(iv == null){
//                    Log.e("null", "iv null");
//                }else{
//                    if(imageArra == null){
//                        Log.e("null", "imageArra null");
//                    }else{
//                        //Log.e("null", "iv not null");
//                        iv.setInAnimation(in);
//                        iv.setOutAnimation(out);
//                        iv.showNext();
//
//                    }
//
//                }
//                iv.postDelayed(this, 3920);
//            }
//        }, 0);

    }

    private void sendReport() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/funnyvideos/ad/report.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("response", response);

                        //new PullMovieInfo().execute(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        snackbar = Snackbar.make(v, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                if (snackbar != null) {
//                                    snackbar.dismiss();
//                                }
//                            }
//                        });
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("deviceid", getDeviceID());
                params.put("action", Containers.ad[8]);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void createUser() {
        TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String uniqueID = "";
        if(tManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
            Log.e("Android ID", Settings.Secure.getString(this.getContentResolver(),Settings
                    .Secure.ANDROID_ID));
            uniqueID = Settings.Secure.getString(this.getContentResolver(),Settings
                    .Secure.ANDROID_ID);
        }else{
            uniqueID = tManager.getDeviceId();
            Log.e("UID", uniqueID);
        }



    }

    public static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    private void initializeAllApi() {

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        account = getAccount(AccountManager.get(getApplicationContext()));
        if (account == null) {
            accountName = "notSet";
            editor.putString("account", accountName);
            editor.commit();
        } else {
            accountName = account.name;
            editor.putString("account", accountName);
            editor.commit();
        }

        Pushbots.sharedInstance().init(this);
        Pushbots.sharedInstance().setAlias(accountName + " " + Build.DEVICE + ": " + Build.MODEL);

        StartAppSDK.init(this, "205136107", true);

        AnalyticsTrackers.initialize(this);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

    }

    @Override
    public void onRefresh() {
        hit(s, e);
    }

    void hit(final int s, final int e) {
        //Log.e("page", s + " " + e);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, test,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("response", response);

                        new PullMovieInfo().execute(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        snackbar = Snackbar.make(v, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                if (snackbar != null) {
//                                    snackbar.dismiss();
//                                }
//                            }
//                        });
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("s", "" + s);
                params.put("e", "" + e);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        startAppAd.showAd(new AdDisplayListener() {
            @Override
            public void adHidden(Ad ad) {

            }

            @Override
            public void adDisplayed(Ad ad) {

            }

            @Override
            public void adClicked(Ad ad) {
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("StartApp Full Screen")
                        .setAction("StartApp Full Clicked " + sp.getString("account", "notset"))
                        .setLabel("Clicked Full " + sp.getString("account", "notset"))
                        .build());
            }

            @Override
            public void adNotDisplayed(Ad ad) {

            }
        });

        //Log.e("asdf", "" + requestCode + " " + resultCode);
    }

    private String getDeviceID() {
        TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String uniqueID = "";
        if(tManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
            Log.e("Android ID", Settings.Secure.getString(this.getContentResolver(),Settings
                    .Secure.ANDROID_ID));
            uniqueID = Settings.Secure.getString(this.getContentResolver(),Settings
                    .Secure.ANDROID_ID);
        }else{
            uniqueID = tManager.getDeviceId();
            Log.e("UID", uniqueID);
        }

        return uniqueID;

    }

    @Override
    public void onBackPressed() {
        startAppAd.onBackPressed();
        super.onBackPressed();
    }

    private class PullMovieInfo extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            title = new ArrayList();
            views = new ArrayList();
            lastid = new ArrayList();
            like = new ArrayList();
            dislike = new ArrayList();
            duration = new ArrayList();
            thumbnail = new ArrayList();
            try {
                JSONObject jsonObject1 = new JSONObject(params[0]);
                JSONArray jArr = jsonObject1.getJSONArray("FullVideos");
                JSONArray totalItems = jsonObject1.getJSONArray("Total");
                JSONObject total = totalItems.getJSONObject(0);
                totalNumberOfVideos = total.getInt("total");
                for (int t = 0; t < jArr.length(); t++) {
                    JSONObject jsonObject = jArr.getJSONObject(t);
                    title.add(jsonObject.get("title"));
                    views.add(jsonObject.get("views"));
                    lastid.add(jsonObject.get("lastid"));
                    like.add(jsonObject.get("like"));
                    dislike.add(jsonObject.get("dislike"));
                    duration.add(jsonObject.get("duration"));
                    thumbnail.add(jsonObject.get("thumbnail"));
                }

            } catch (JSONException e) {
                //Log.e("error check", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            LVAdapter adapter = new LVAdapter(MainActivity.this, title, views, lastid, like, dislike, duration, thumbnail);
            //ScaleInAnimatorAdapter animatorAdapter = new ScaleInAnimatorAdapter(adapter, lv);
            lv.setAdapter(adapter);
            int button = totalNumberOfVideos / 50;
            if (totalNumberOfVideos % 50 != 0) {
                button++;
            }
            button = button - mLinearScroll.getChildCount();
            setReadyPagination(button);

//            if (sp.getLong("total", -1) < totalNumberOfVideos) {
//                //MenuItem New = actionbarMenu.findItem(R.id.action_settings);
//                actionbarMenu.setVisible(true);
//            } else {
//                actionbarMenu.setVisible(false);
//            }

            swipeRefreshLayout.setRefreshing(false);
            //itemNumbers(genre);

            //ad.show();
        }

        private void setReadyPagination(int button) {
            if (title != null && title.size() != 0) {
                int size = (totalNumberOfVideos / 50) + 1;

                for (int j = 0; j < button; j++) {
                    final int k;
                    k = j;
                    final Button btnPage = new Button(MainActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(120,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, 0, 0, 0);
                    btnPage.setTextColor(Color.WHITE);
                    btnPage.setTextSize(13.0f);
                    btnPage.setId(j);
                    btnPage.setText(String.valueOf(mLinearScroll.getChildCount() + 1));
                    btnPage.setBackgroundResource(R.drawable.button_unfocused);
                    if (j == sp.getInt("lastPage", 0)) {
                        previousClicked = btnPage;
                        btnPage.setBackgroundResource(R.drawable.button_focus);
                        previousClicked.setBackgroundResource(R.drawable.button_focus);
                        btnPage.setTextColor(Color.parseColor("#000000"));
                    }
                    mLinearScroll.addView(btnPage, lp);


                    btnPage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            previousClicked.setBackgroundResource(R.drawable.pagebuttontranssss);
                            previousClicked.setTextColor(Color.parseColor("#ffffff"));

                            btnPage.setBackgroundResource(R.drawable.pagebuttontrans);
                            btnPage.setTextColor(Color.parseColor("#000000"));
                            TransitionDrawable transition = (TransitionDrawable) btnPage.getBackground();
                            transition.startTransition(250);
                            TransitionDrawable transition2 = (TransitionDrawable) previousClicked.getBackground();
                            transition2.startTransition(250);
                            previousClicked = btnPage;
                            s = (30 * btnPage.getId());
                            e = ((30 * (btnPage.getId() + 1)) - 1);

                            editor.putInt("s", s);
                            editor.putInt("e", e);
                            editor.putInt("lastPage", btnPage.getId());
                            editor.putInt("scroll", scroll.getScrollX());
                            Log.e("scroll", "" + scroll.getScrollX() + " " + scroll.getScrollY() );
                            editor.commit();

                            //Log.e("adfdsafadsfasdf", e + "");
                            onRefresh = "http://android.teknobilim.org/Movie/?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=" + (30 * Integer.parseInt("" + btnPage.getId())) + "&f=" + ((30 * Integer.parseInt("" + (btnPage.getId() + 1))) - 1) + "&id";
                            //new PullMovieInfo().execute(onRefresh);
                            hit(s, e);

                        }
                    });
                }

            }

            //Toast.makeText(MainActivity.this, sp.getInt("scroll", 0), Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //getNotice();
            swipeRefreshLayout.setRefreshing(true);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd");
            String formattedDate = df.format(c.getTime());
            int date = Integer.parseInt(formattedDate);
            if (date % sp.getInt("reviewCycle", 3) == 0) {
                if (sp.getBoolean("review", true)) {
                    editor.putBoolean("review", false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setTitle("REVIEW FREE MOVIE ONLINE")
                            .setMessage("Please rate this app. Your review will help us to improve our FREE MOVIE ONLINE.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                        //String url = "http://www.amazon.com/TriumphIT-Music-Player/dp/B00ZD2D1T2/ref=sr_1_16?s=mobile-apps&ie=UTF8&qid=1434885049&sr=1-16&keywords=amazon+mp3&pebp=1434885051870&perid=10M3Z3SR7RE5RBMZ1M1N";
                                        //Intent i = new Intent(Intent.ACTION_VIEW);
                                        //i.setData(Uri.parse(url));
                                        //startActivity(i);
                                    }
                                    editor.putInt("reviewCycle", 30);// do nothing
                                    editor.commit();
                                    finish();
                                }
                            })
                            .setNegativeButton("Never", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    editor.putInt("reviewCycle", 20);// do nothing
                                    editor.commit();
                                    finish();
                                }
                            }).setNeutralButton("Later", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            editor.putInt("reviewCycle", 3);// do nothing
                            editor.commit();
                            finish();
                        }
                    })
                            .setIcon(R.mipmap.ic_launcher);

                    final AlertDialog a = builder.create();
                    a.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialog) {
                            a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            a.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.colorPrimary));
                            a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                    });
                    a.show();
                    editor.commit();

                } else {
                    finish();
                }
                //moveTaskToBack(false);

                return true;
            } else {
                editor.putBoolean("review", true);
                editor.commit();
            }
            //onBackPressed();

        }
        return super.onKeyDown(keyCode, event);
    }

}
