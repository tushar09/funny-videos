package tech.triumphit.funnyvideos.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.tagmanager.Container;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tech.triumphit.funnyvideos.AnalyticsTrackers;
import tech.triumphit.funnyvideos.Containers;
import tech.triumphit.funnyvideos.MainActivity;
import tech.triumphit.funnyvideos.Player;
import tech.triumphit.funnyvideos.R;

/**
 * Created by Tushar on 6/21/2016.
 */
public class LVAdapter extends BaseAdapter{

    ArrayList title, views, lastid, like, dislike, duration, thumbnail;
    Context context;
    LayoutInflater inflater;
    static Animation animation;
    private int lastPosition = -1;
    private Tracker t;
    private SharedPreferences sp;

    public LVAdapter(Context context, ArrayList title, ArrayList views, ArrayList lastid, ArrayList like, ArrayList dislike, ArrayList duration, ArrayList thumbnail) {
        this.context = context;
        this.title = title;
        this.views = views;
        this.lastid = lastid;
        this.like = like;
        this.dislike = dislike;
        this.duration = duration;
        this.thumbnail = thumbnail;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
        AnalyticsTrackers.initialize(context);
        //at = AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        t = AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
    }

    @Override
    public int getCount() {
        return title.size();
    }

    @Override
    public Object getItem(int position) {
        return title.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (animation == null) {
            animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        }

        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.funnyvideos_row, null);
            holder = new Holder();
            holder.cv = (CardView) convertView.findViewById(R.id.view);
            holder.title = (TextView) convertView.findViewById(R.id.textView4);
            holder.views = (TextView) convertView.findViewById(R.id.textView5);
            holder.like = (TextView) convertView.findViewById(R.id.textView7);
            holder.dislike = (TextView) convertView.findViewById(R.id.textView8);
            holder.duration = (TextView) convertView.findViewById(R.id.textView3);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
            holder.likeThmb = (ImageButton) convertView.findViewById(R.id.imageButton);
            holder.dislikeThmb = (ImageButton) convertView.findViewById(R.id.imageButton2);
            if(Containers.liked.contains("" + lastid.get(position))){
                holder.likeThmb.setBackgroundResource(R.drawable.likecolor);
                holder.likeThmb.setTag(R.drawable.likecolor);
                holder.dislikeThmb.setTag(R.drawable.dislike1);
            }else{
                holder.likeThmb.setBackgroundResource(R.drawable.like1);
                holder.likeThmb.setTag(R.drawable.like1);
                holder.dislikeThmb.setTag(R.drawable.dislikecoloer);
            }
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();

            if(Containers.liked.contains("" + lastid.get(position))){
                holder.likeThmb.setBackgroundResource(R.drawable.likecolor);
                holder.likeThmb.setTag(R.drawable.likecolor);
            }else{
                holder.likeThmb.setBackgroundResource(R.drawable.like1);
                holder.likeThmb.setTag(R.drawable.like1);
            }

            if(Containers.dislike.contains("" + lastid.get(position))){
                holder.dislikeThmb.setBackgroundResource(R.drawable.dislikecoloer);
                holder.dislikeThmb.setTag(R.drawable.dislikecoloer);
            }else{
                holder.dislikeThmb.setBackgroundResource(R.drawable.dislike1);
                holder.dislikeThmb.setTag(R.drawable.dislike1);
            }
        }
        holder.title.setText("" + title.get(position));
        holder.views.setText("Views: " + views.get(position));
        holder.like.setText("" + like.get(position));
        holder.dislike.setText("" + dislike.get(position));
        holder.duration.setText("" + duration.get(position));
        Picasso.with(context).load("" + thumbnail.get(position)).fit().centerCrop().into(holder.thumbnail);

        holder.likeThmb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //like.set(position, Integer.parseInt("" + like.get(position)) + 1);
                //holder.like.setText("" + like.get(position));
                setLike(position, holder);
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Like " + title.get(position))
                        .setAction(getDeviceID() + sp.getString("account", "notset"))
                        .setLabel(getDeviceID() + sp.getString("account", "notset"))
                        .build());

            }
        });

        holder.dislikeThmb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //like.set(position, Integer.parseInt("" + like.get(position)) + 1);
                //holder.like.setText("" + like.get(position));
                setDisLike(position, holder);
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("Dislike " + title.get(position))
                        .setAction(getDeviceID() + sp.getString("account", "notset"))
                        .setLabel(getDeviceID() + sp.getString("account", "notset"))
                        .build());

            }
        });

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //context.startActivity(new Intent(context.getApplicationContext(), Player.class).putExtra("id", "" + lastid.get(position)));
                Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) context, "AIzaSyCqMfBCb5u5h4IAWdJ8q_ID9l4qzO7l7mY" , "" + lastid.get(position), 0,  true, false);
                //YouTubeStandalonePlayer.getReturnedInitializationResult(intent);
                ((Activity) context).startActivityForResult(intent, 5589);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/funnyvideos/setWatched.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("erro", response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Funny Videos")
                                        .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        })
                                        .setIcon(R.mipmap.ic_launcher);
                                final AlertDialog a = builder.create();
                                a.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                                    }
                                });
                                a.show();
                                //dfinish();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("deviceid", getDeviceID());
                        params.put("video_link", "https://www.youtube.com/watch?v=" + lastid.get(position));
                        params.put("video_lastid", "" + lastid.get(position));
                        return params;
                    }

                };
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(stringRequest);
            }
        });

        t.send(new HitBuilders.EventBuilder()
                .setCategory("View " + title.get(position))
                .setAction(getDeviceID() + sp.getString("account", "notset"))
                .setLabel(getDeviceID() + sp.getString("account", "notset"))
                .build());

        return convertView;
    }

    private void setLike(final int position, final Holder holder) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/funnyvideos/setLiked.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("maintenance", response + " sdfasdfasdf ");
                        //new PullMovieInfo().execute(response);
                        if(response.equals("red icon")){
                            //like.set(position, Integer.parseInt("" + like.get(position)) - 1);
                            like.set(position, Integer.parseInt("" + like.get(position)) + 1);
                            if(holder.dislikeThmb.getTag().toString().equals(R.drawable.dislikecoloer + "")){
                                dislike.set(position, Integer.parseInt("" + dislike.get(position)) - 1);
                                holder.dislike.setText("" + dislike.get(position));
                                holder.dislikeThmb.setBackgroundResource(R.drawable.dislike1);
                                holder.dislikeThmb.setTag(R.drawable.dislike1);
                            }
                            holder.like.setText("" + like.get(position));
                            holder.likeThmb.setBackgroundResource(R.drawable.likecolor);
                            holder.likeThmb.setTag(R.drawable.likecolor);
                            Containers.liked.add("" + lastid.get(position));
                            Log.e("asdf", "added " + response);
                        }else{
                            like.set(position, Integer.parseInt("" + like.get(position)) - 1);
                            holder.like.setText("" + like.get(position));
                            holder.likeThmb.setBackgroundResource(R.drawable.like1);
                            holder.likeThmb.setTag(R.drawable.like1);
                            //holder.dislikeThmb.setBackgroundResource(R.drawable.dislike1);
                            Containers.liked.remove("" + lastid.get(position));
                            Log.e("asdf", "removed " + response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Funny Videos")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("deviceid", getDeviceID());
                params.put("video_link", "https://www.youtube.com/watch?v=" + lastid.get(position));
                params.put("video_lastid", "" + lastid.get(position));
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void setDisLike(final int position, final Holder holder) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/funnyvideos/setDisLiked.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("maintenance", response + " sdfasdfasdf ");
                        //new PullMovieInfo().execute(response);
                        if(response.equals("red icon")){

                            dislike.set(position, Integer.parseInt("" + dislike.get(position)) + 1);
                            if(holder.likeThmb.getTag().toString().equals(R.drawable.likecolor + "")){
                                like.set(position, Integer.parseInt("" + like.get(position)) - 1);
                                holder.like.setText("" + like.get(position));
                                holder.likeThmb.setBackgroundResource(R.drawable.like1);
                                holder.likeThmb.setTag(R.drawable.like1);

                            }
                            holder.dislike.setText("" + dislike.get(position));

                            holder.dislikeThmb.setBackgroundResource(R.drawable.dislikecoloer);
                            holder.dislikeThmb.setTag(R.drawable.dislikecoloer);
                            Containers.dislike.add("" + lastid.get(position));
                            Log.e("asdf", "added " + response);
                        }else{
                            dislike.set(position, Integer.parseInt("" + dislike.get(position)) - 1);
                            //dislike.set(position, Integer.parseInt("" + like.get(position)) - 1);
                            holder.dislike.setText("" + dislike.get(position));
                            holder.dislikeThmb.setBackgroundResource(R.drawable.dislike1);
                            //holder.dislikeThmb.setBackgroundResource(R.drawable.dislike1);
                            holder.dislikeThmb.setTag(R.drawable.dislike1);
                            Containers.dislike.remove("" + lastid.get(position));
                            Log.e("asdf", "removed " + response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Funny Videos")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        //finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("deviceid", getDeviceID());
                params.put("video_link", "https://www.youtube.com/watch?v=" + lastid.get(position));
                params.put("video_lastid", "" + lastid.get(position));
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

//    private void nutralLiked(final int position, final Holder holder) {
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/funnyvideos/nutralLiked.php",
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.e("maintenance", response + " sdfasdfasdf ");
//                        //new PullMovieInfo().execute(response);
//                        if(Integer.parseInt(response) == 1){
//                            like.set(position, Integer.parseInt("" + like.get(position)) + 1);
//                            holder.like.setText("" + like.get(position));
//                            Containers.liked.add("" + lastid.get(position));
//                            Log.e("asdf", "asdf like found");
//                        }else{
//                            deleteLiked(position, holder);
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                        builder.setTitle("Funny Videos")
//                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
//                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        //finish();
//                                    }
//                                })
//                                .setIcon(R.mipmap.ic_launcher);
//                        final AlertDialog a = builder.create();
//                        a.setOnShowListener(new DialogInterface.OnShowListener() {
//                            @Override
//                            public void onShow(DialogInterface dialog) {
//                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.colorPrimary));
//                            }
//                        });
//                        a.show();
//                        //dfinish();
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("deviceid", getDeviceID());
//                params.put("video_link", "https://www.youtube.com/watch?v=" + lastid.get(position));
//                params.put("video_lastid", "" + lastid.get(position));
//                return params;
//            }
//
//        };
//        RequestQueue requestQueue = Volley.newRequestQueue(context);
//        requestQueue.add(stringRequest);
//    }

    private String getDeviceID() {
        TelephonyManager tManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String uniqueID = "";
        if(tManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
            Log.e("Android ID", Settings.Secure.getString(context.getContentResolver(),Settings
                    .Secure.ANDROID_ID));
            uniqueID = Settings.Secure.getString(context.getContentResolver(),Settings
                    .Secure.ANDROID_ID);
        }else{
            uniqueID = tManager.getDeviceId();
            Log.e("UID", uniqueID);
        }

        return uniqueID;

    }



    class Holder{
        TextView title, views, like, dislike, duration;
        ImageButton likeThmb, dislikeThmb;
        ImageView thumbnail;
        CardView cv;
    }

}
