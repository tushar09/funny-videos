package tech.triumphit.funnyvideos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tech.triumphit.funnyvideos.adapter.LVAdapter;

public class LikedAndWatched extends AppCompatActivity {

    private ArrayList title;
    private ArrayList lastid;
    private ArrayList like, dislike, duration, thumbnail;
    private ArrayList views;

    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liked_and_watched);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lv = (ListView) findViewById(R.id.listView);
        getWatchedVideos();
        getSupportActionBar().setTitle(getIntent().getStringExtra("title"));
    }

    public void getWatchedVideos() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, getIntent().getStringExtra("link"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("maintenance", response + " sdfasdfasdf ");
                        new PullMovieInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(LikedAndWatched.this);
                        builder.setTitle("Funny Videos")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("deviceid", getDeviceID());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private String getDeviceID() {
        TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String uniqueID = "";
        if(tManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
            Log.e("Android ID", Settings.Secure.getString(this.getContentResolver(),Settings
                    .Secure.ANDROID_ID));
            uniqueID = Settings.Secure.getString(this.getContentResolver(),Settings
                    .Secure.ANDROID_ID);
        }else{
            uniqueID = tManager.getDeviceId();
            Log.e("UID", uniqueID);
        }

        return uniqueID;

    }

    private class PullMovieInfo extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            title = new ArrayList();
            views = new ArrayList();
            lastid = new ArrayList();
            like = new ArrayList();
            dislike = new ArrayList();
            duration = new ArrayList();
            thumbnail = new ArrayList();
            try {
                JSONObject jsonObject1 = new JSONObject(params[0]);
                JSONArray jArr = jsonObject1.getJSONArray("FullVideos");
                for (int t = 0; t < jArr.length(); t++) {
                    JSONObject jsonObject = jArr.getJSONObject(t);
                    title.add(jsonObject.get("title"));
                    views.add(jsonObject.get("views"));
                    lastid.add(jsonObject.get("video_lastid"));
                    like.add(jsonObject.get("like"));
                    dislike.add(jsonObject.get("dislike"));
                    duration.add(jsonObject.get("duration"));
                    thumbnail.add(jsonObject.get("thumbnail"));
                }

            } catch (JSONException e) {
                Log.e("error check", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            LVAdapter adapter = new LVAdapter(LikedAndWatched.this, title, views, lastid, like, dislike, duration, thumbnail);
            //ScaleInAnimatorAdapter animatorAdapter = new ScaleInAnimatorAdapter(adapter, lv);
            lv.setAdapter(adapter);

            //swipeRefreshLayout.setRefreshing(false);
            //itemNumbers(genre);

            //ad.show();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //getNotice();
            //swipeRefreshLayout.setRefreshing(true);
        }
    }
}
