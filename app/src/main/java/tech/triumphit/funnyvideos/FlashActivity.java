package tech.triumphit.funnyvideos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tech.triumphit.funnyvideos.adapter.LVAdapter;

public class FlashActivity extends AppCompatActivity {

    String maintenanceMode = "";
    ProgressBar pb;
    RoundCornerProgressBar pb1;
    private ArrayList lastid;
    String [] ad;
    //private void likedVideos;
    //Round

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);
        pb1 = (RoundCornerProgressBar) findViewById(R.id.pb);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        //hit();
        checkMaintenance();
    }

    void checkMaintenance() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/funnyvideos/maintenance.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("maintenance");
                            JSONObject mode = jArr.getJSONObject(0);
                            maintenanceMode = mode.getString("maintenancemode");
                            if(maintenanceMode.equals("yes")){
                                AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                                builder.setTitle("Funny Videos")
                                        .setMessage("Sorry, server under maintenance. Please wait for 1 hour.")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        })
                                        .setIcon(R.mipmap.ic_launcher);
                                final AlertDialog a = builder.create();
                                a.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                                    }
                                });
                                a.show();
                                //Log.e("maintenance", maintenanceMode + " from run");
                            }else if(maintenanceMode.equals("no")){
                                pb1.setProgress(20);
                                //Intent i = new Intent(FlashActivity.this, MainActivity.class);
                                //startActivity(i);
                                //finish();
                                createUser();
                            }
                            //("maintenance", maintenanceMode);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                        builder.setTitle("FREE MOVIE ONLINE")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void createUser() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/funnyvideos/createuser.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("maintenance", response);
                        pb1.setProgress(50);
                        getLikedVideos();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                        builder.setTitle("Funny Videos")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("deviceid", getDeviceID());
                params.put("username", "user");
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void getLikedVideos() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/funnyvideos/liked.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("maintenance", response);
                        pb1.setProgress(80);
                        new PullMovieInfo().execute(response, "lik");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                        builder.setTitle("Funny Videos")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("deviceid", getDeviceID());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void getDisLikedVideos() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/funnyvideos/disliked.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("maintenance", response);
                        pb1.setProgress(95);
                        new PullMovieInfo().execute(response, "dis");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                        builder.setTitle("Funny Videos")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("deviceid", getDeviceID());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public void getAds() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/funnyvideos/ad/ad.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("maintenance", response + "sdfgdfgsdg");
                        pb1.setProgress(100);
                        new PullMovieInfo().execute(response, "ad");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FlashActivity.this);
                        builder.setTitle("Funny Videos")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private String getDeviceID() {
        TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String uniqueID = "";
        if(tManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE){
            //Log.e("Android ID", Settings.Secure.getString(this.getContentResolver(),Settings
                    //.Secure.ANDROID_ID));
            uniqueID = Settings.Secure.getString(this.getContentResolver(),Settings
                    .Secure.ANDROID_ID);
        }else{
            uniqueID = tManager.getDeviceId();
            //Log.e("UID", uniqueID);
        }

        return uniqueID;

    }

    private class PullMovieInfo extends AsyncTask<String, String, String> {

        String check = "";

        @Override
        protected String doInBackground(String... params) {

            check = params[1];

            lastid = new ArrayList();
            //Containers.liked = new ArrayList();
            try {
                JSONObject jsonObject1 = new JSONObject(params[0]);
                if(params[1].equals("ad")){
                    ad = new String[9];
                    JSONArray jArr = jsonObject1.getJSONArray("ad");
                    JSONObject jsonObject = jArr.getJSONObject(0);
                    Containers.ad [0] = "" + jsonObject.get("id");
                    Containers.ad [1] = "" + jsonObject.get("title");
                    Containers.ad [2] = "" + jsonObject.get("subtitle");
                    Containers.ad [3] = "" + jsonObject.get("icon");
                    Containers.ad [4] = "" + jsonObject.get("pic1");
                    Containers.ad [5] = "" + jsonObject.get("pic2");
                    Containers.ad [6] = "" + jsonObject.get("pic3");
                    Containers.ad [7] = "" + jsonObject.get("pic4");
                    Containers.ad [8] = "" + jsonObject.get("action");
                    Log.e("adsfasdfasdfasdf", Containers.ad [0]);
                    Log.e("adsfasdfasdfasdf", Containers.ad [1]);
                    Log.e("adsfasdfasdfasdf", Containers.ad [2]);
                    Log.e("adsfasdfasdfasdf", Containers.ad [3]);
                    Log.e("adsfasdfasdfasdf", Containers.ad [4]);
                    Log.e("adsfasdfasdfasdf", Containers.ad [5]);
                    Log.e("adsfasdfasdfasdf", Containers.ad [6]);
                    Log.e("adsfasdfasdfasdf", Containers.ad [7]);
                    Log.e("adsfasdfasdfasdf", Containers.ad [8]);

//                    for (int t = 0; t < jArr.length(); t++) {
//                        JSONObject jsonObject = jArr.getJSONObject(t);
//
//                        lastid.add(jsonObject.get("id"));
//
//                    }
                }else{
                    JSONArray jArr = jsonObject1.getJSONArray("FullVideos");
                    for (int t = 0; t < jArr.length(); t++) {
                        JSONObject jsonObject = jArr.getJSONObject(t);

                        lastid.add(jsonObject.get("video_lastid"));

                    }
                }


                if(params[1].equals("lik")){
                    Containers.liked = new ArrayList(lastid);
                }else if(params[1].equals("dis")){
                    Containers.dislike = new ArrayList(lastid);
                }

            } catch (JSONException e) {
                Log.e("error check", e.toString() + " " + params[1]);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(check.equals("lik")){
                getDisLikedVideos();
            }else if(check.equals("dis")){
                getAds();
            }else if(check.equals("ad")){
                startActivity(new Intent(FlashActivity.this, MainActivity.class));

                finish();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //getNotice();
            //swipeRefreshLayout.setRefreshing(true);
        }
    }

}
